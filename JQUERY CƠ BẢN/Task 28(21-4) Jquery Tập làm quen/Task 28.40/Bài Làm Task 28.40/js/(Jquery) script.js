"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gREADY_STATE_REQUEST_DONE = 4;
const gSTATUS_REQUEST_DONE = 200;

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */


/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

// Hàm này để xử lý sự kiện nút check voucher click
$('#btn-check-voucher').on('click', function() {
    "user strict";
    var vInputVoucher = $('#voucher'); // truy vấn ô input
    var vDivResultCheck = $('#div-result-check'); // truy vấn thẻ div qua id
    console.log('Tôi lấy dữ liệu từ');
    console.log('Input, id = ' + vInputVoucher.attr('id') + '- placeholder = ' + vInputVoucher.attr('placeholder'));
    console.log('Tác động vào');
    console.log('Div, id = ' + vDivResultCheck.attr('id') + ' - innerHTML = ' + vDivResultCheck.html());

    // Khai báo đối tượng voucher để sử dụng trong các bước
    var vVoucherObj = {
            maGiamGia: ""
        }
        // Bước 1: lấy giá trị nhập trên form, mã voucher (thu thập dữ liệu)
    var vVoucherCode = readData(vVoucherObj);

    //Bước 2: ValiDate data
    var vIsValidateData = validateData(vVoucherObj);
    if (vIsValidateData) {

        // Bước 3: gọi sever
        var vXmlHttp = new XMLHttpRequest();
        sendVoucherToSever(vVoucherObj, vXmlHttp);

        // Bước 4: xử lý hiển thị
        vXmlHttp.onreadystatechange = function() {
            if (vXmlHttp.readyState === gREADY_STATE_REQUEST_DONE &&
                vXmlHttp.status === gSTATUS_REQUEST_DONE) {
                processResponse(vXmlHttp);
            }
        }
    }
});

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

// Hàm đọc dữ liệu từ ô Input
function readData(paramVoucherObject) {
    "use strict";
    var vInputVoucher = $('#voucher');
    var vValueVoucher = vInputVoucher.val();
    paramVoucherObject.maGiamGia = vValueVoucher;
}

// Hàm validate dữ liệu
function validateData(paramVoucherObject) {
    var vDivResultCheck = $('#div-result-check'); //Truy vấn thẻ div qua id
    if (paramVoucherObject.maGiamGia === "") {

        // Hiển thị câu thông báo lỗi ra
        vDivResultCheck.html("Mã giảm giá chưa nhập!");

        // Thay đổi class css, để đổi màu chữ thành màu đỏ
        vDivResultCheck.prop("class", "text-danger");
        return false;
    }

    // Thay đổi class css, để đổi màu chữ thành màu đen bình thường
    vDivResultCheck.prop('class', 'text-darnk');
    return true;
}

// Ham thuc hien viec call api va gui ma voucher ve server
function sendVoucherToSever(paramVoucherObject, paramXmlVoucherRequest) {
    paramXmlVoucherRequest.open("GET", "http://42.115.221.44:8080/devcamp-voucher-api/vouchers/" + paramVoucherObject.maGiamGia, true);
    paramXmlVoucherRequest.send();
}

// Hàm này được dùng để xử lý khi server trả về response
function processResponse(paramXmlHttp) {

    // B1: nhận lại response dạng JSON ở xmlHttp.responseText
    var vJsonVoucherResponse = paramXmlHttp.responseText;
    // lấy responseText ghi ra console
    console.log(vJsonVoucherResponse);

    // B2: Parsing chuỗi JSON thành Object
    var vVoucherResObj = JSON.parse(vJsonVoucherResponse);
    console.log('%cTest 3', 'color: orangered');
    console.log(vJsonVoucherResponse);

    // B3: xử lý mã giảm giá dựa vào đối tượng vừa có
    var vDiscount = vVoucherResObj.discount;
    var vDivResultCheck = $('#div-result-check');

    //discount - -1 nghĩa là mã giảm giá không tồn tại
    if (vDiscount === "-1") {
        vDivResultCheck.html("Không tồn tại mã giảm giá");
    } else {
        vDivResultCheck.html("Mã giảm giá " + vDiscount + "%");
    }
}