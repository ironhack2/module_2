    "use strict";

    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

    var gStudentsData = [];
    var gSubjectData = [];
    var gGrades = [];
    const gTableStudentData = ["id", "studentId", "subjectId", "grade", "examDate", "action"];
    const gID_COL = 0;
    const gSTUDENT_COL = 1;
    const gSUBJECT_COL = 2;
    const gGRADE_COL = 3;
    const gDATE_COL = 4;
    const gACTION_COL = 5;

    var gUserId = {
        id: '',
    }

    var gStudentId = [];
    var gSubjectId = [];
    var gStt = 0;

    var gStudentTable = $("#student-table").DataTable({
        columns: [{
            data: gTableStudentData[gID_COL]
        }, {
            data: gTableStudentData[gSTUDENT_COL]
        }, {
            data: gTableStudentData[gSUBJECT_COL]
        }, {
            data: gTableStudentData[gGRADE_COL]
        }, {
            data: gTableStudentData[gDATE_COL]
        }, {
            data: gTableStudentData[gACTION_COL]
        }, ],
        columnDefs: [{
            targets: gID_COL,
            render: function() {
                return gStt++;
            }
        }, {
            targets: gSTUDENT_COL,
            render: function(data) {
                return renderStudentData(data)
            }
        }, {
            targets: gSUBJECT_COL,
            render: function(data) {
                return renderSubjectData(data)
            }
        }, {
            targets: gACTION_COL,
            defaultContent: "<button class='btn btn-primary btn-update'><i class='fa-solid fa-pencil'></i> Sửa</button> <button class='btn btn-danger btn-delete'><i class='fa-solid fa-trash-can'></i> Xóa</button>"
        }]
    });


    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

    $(document).ready(function() {

        //FILTER
        $("#btn-filter").on('click', function() {
            onBtnClickFilter();
        });

        //ADD NEW
        //  C: Gán sự kiện Create - Thêm mới student
        $("#btn-add-student").on('click', function() {
            onBtnShowCreate();
        });
        // gán sự kiện cho nút Create Grades (trên modal)
        $("#btn-insert-grades").on('click', function() {
            onBtnClickInsert();
        });

        //UP DATE
        // U: gán sự kiện Update - Sửa 1 user
        $(document).on('click', '#student-table .btn-update', function() {
            onBtnClickUpdate(this);
        });
        // gán sự kiện cho nút Update Grades (trên modal)
        $("#btn-update-user").on('click', function() {
            onBtnUpdateUser();
        });

        //DELETE
        $(document).on('click', '#student-table .btn-delete', function() {
            onBtnClickDelete(this);
        });
        $("#btn-confirm-delete").on('click', function() {
            confirmDeleteStudent();
        });


        onPageLoading();
        loadStudentSelect();
        loadSubjectSelect();
        select2TranFormation();
    });


    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

    function onPageLoading() {

        callApiAllGrades();
        callApiServerStudents();
        callApiServerSubject();
        loadDataToTable(gGrades);
        console.log(gGrades);
    }

    /////////   FILTER ////////
    function onBtnClickFilter() {

        // lấy dữ liệu lọc
        var vFilterStudent = {
            student: "",
            subject: "",
        }
        readDataFilter(vFilterStudent);
        var vFilterResults = filterProcess(vFilterStudent);
        loadDataToTable(vFilterResults);
    }

    // hàm lấy dữ liệu filter
    function readDataFilter(paramFilterStudent) {
        paramFilterStudent.student = $("#student-select option:selected").val();
        paramFilterStudent.subject = $("#subject-select option:selected").val();
    }

    // hàm lọc dữ liệu
    function filterProcess(paramFilterStudent) {
        var vFilter = [];
        vFilter = gGrades.filter(function(paramHistoryObject) {
            return ((paramHistoryObject.studentId == paramFilterStudent.student || paramFilterStudent.student == 0) &&
                (paramHistoryObject.subjectId == paramFilterStudent.subject || paramFilterStudent.subject == 0));
        })
        return vFilter;
    }




    //////////////////////////////    ADD NEW AND  CREATE        //////////////////////////////
    //NÚT ADD NEW
    function onBtnShowCreate() {
        $("#create-user").modal('show');
        loadSelectToCreatModal();
    }

    //Hàm load dữ liệu ô select vào create modal
    function loadSelectToCreatModal() {
        $("#create-select-student").append($("<option>", {
            text: "---Chọn học sinh---",
            value: 0
        }));
        for (var i = 0; i < gStudentsData.length; i++) {
            $("#create-select-student").append($("<option>", {
                text: gStudentsData[i].firstname + " " + gStudentsData[i].lastname,
                value: gStudentsData[i].id
            }));
        }
        $("#create-select-subject").append($("<option>", {
            text: "---Chọn môn học---",
            value: 0
        }));
        for (var i = 0; i < gSubjectData.length; i++) {
            $("#create-select-subject").append($("<option>", {
                text: gSubjectData[i].subjectName,
                value: gSubjectData[i].id
            }));
        }
        $("#create-select-grades").append($("<option>", {
            text: "---Chọn điểm thi---",
            value: 0
        }));
        for (var i = 0; i < gGrades.length; i++) {
            $("#create-select-grades").append($("<option>", {
                text: gGrades[i].grade,
                value: gGrades[i].id
            }));
        }
        $("#create-select-date").append($("<option>", {
            text: "---Chọn ngày thi---",
            value: 0
        }));
        for (var i = 0; i < gGrades.length; i++) {
            $("#create-select-date").append($("<option>", {
                text: gGrades[i].examDate,
                value: gGrades[i].id
            }));
        }
    }


    // CÁC HÀM CREATE - TẠO MỚI 1 STUDENT
    //Hàm nút create
    function onBtnClickInsert() {
        var vStudentCreate = {
            studentId: "",
            subjectId: "",
            grade: "",
            examDate: "",
        }
        readDataCreate(vStudentCreate);
        var vKiemTra = validateStudent(vStudentCreate);
        console.log(vStudentCreate);
        if (vKiemTra) {
            callApiCreateNewGrade(vStudentCreate);
            loadDataToTable(gGrades);
            $("#update-user-modal").modal("hide");
        }
    }

    // Hàm đọc dữ liệu
    function readDataCreate(paramStudentCreate) {
        paramStudentCreate.studentId = $("#create-select-student").val();
        paramStudentCreate.subjectId = $("#create-select-subject").val()
        paramStudentCreate.grade = $("#create-select-grades option:selected").html()
        paramStudentCreate.examDate = $("#create-select-date option:selected").html()
    }

    //Hàm validate
    function validateStudent(paramStudentCreate) {
        if (paramStudentCreate.name === "0") {
            alert("Chưa chọn học sinh");
            return false;
        }
        if (paramStudentCreate.monhoc === "0") {
            alert("Chưa chọn môn học");
            return false;
        }
        if (paramStudentCreate.diem === "0") {
            alert("Chưa chọn điểm thi");
            return false;
        }
        if (paramStudentCreate.ngaythi === "0") {
            alert("Chưa chọn ngày Thi");
            return false;
        }
        return true;
    }

    //Hàm call API
    function callApiCreateNewGrade(paramObj) {
        $.ajax({
            url: "https://62454a477701ec8f724fb923.mockapi.io/api/v1/" + "/grades",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=UTF-8",
            data: JSON.stringify(paramObj),
            async: true,
            success: function(responseJson) {
                var vResponseString = JSON.stringify(responseJson);
                alert("Thêm user thành công" + vResponseString);
                location.reload();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(ajaxContext);
            }
        });
    }


    //////////////////////////////    EDIT AND  UPDATE        //////////////////////////////
    //NÚT EDIT
    function onBtnClickUpdate(paramUpdate) {
        var vCurrentRow = $(paramUpdate).closest("tr");
        var vTable = $("#student-table").DataTable();
        var vDataResult = vTable.row(vCurrentRow).data();
        console.log(vDataResult);
        gUserId.id = vDataResult.id;
        loadSelectToModalUpdate(vDataResult);
        $("#update-student-modal").modal('show');
    }

    // CÁC HÀM UPDATE - SỬA 1 STUDENT
    //Hàm nút UPDATE
    function onBtnUpdateUser() {
        var gStudentModal = {
            id: "",
            studentId: "",
            subjectId: "",
            examDate: "",
            grade: "",
        }
        readUpdateStudent(gStudentModal);
        callApiUpdateStudentModal(gStudentModal);
        loadDataToTable(gGrades);
        var vString = JSON.stringify(gStudentModal);
        alert("cập nhật thành công" + vString);
        $("#update-student-modal").modal('hide');
        location.reload();
    }

    //Hàm đọc dữ liệu
    function readUpdateStudent(paramUpdateUser) {
        paramUpdateUser.id = gUserId.id;
        paramUpdateUser.studentId = $("#update-student option:selected").val();
        paramUpdateUser.subjectId = $("#update-subject option:selected").val();
        paramUpdateUser.examDate = $("#update-date option:selected").html();
        paramUpdateUser.grade = $("#update-mark option:selected").html();

    }

    //Hàm call API
    function callApiUpdateStudentModal(paramUpdateUser) {
        $.ajax({
            url: "https://62454a477701ec8f724fb923.mockapi.io/api/v1/" + "/grades/" + paramUpdateUser.id,
            dataType: "json",
            type: "PUT",
            contentType: "application/json; charset=UTF-8",
            data: JSON.stringify(paramUpdateUser),
            success: function(responseJson) {
                console.log(responseJson);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert("Không thành công");
            }
        });
    }



    //////////////////////////////                    DELETE                           //////////////////////////////

    // NÚT DELETE
    function onBtnClickDelete(paramDelete) {
        var vCurrentRow = $(paramDelete).closest("tr");
        var vTable = $("#student-table").DataTable();
        var vDataResult = vTable.row(vCurrentRow).data();
        console.log(vDataResult);
        gUserId.id = vDataResult.id;
        $("#delete-confirm-modal").modal('show')
    }

    //NÚT DELETE CONFIRM
    function confirmDeleteStudent() {
        callApiServerDelete();
    }

    //call API DELETE
    function callApiServerDelete() {
        $.ajax({
            url: "https://62454a477701ec8f724fb923.mockapi.io/api/v1/" + "/grades/" + gUserId.id,
            type: "DELETE",
            contentType: "application/json",
            success: function() {
                location.reload();
            },
            error: function(res) {
                alert(res.responseText);
            }
        })
    }



    //////////////////           CÁC HÀM LOAD DỮ LIỆU VÀO BẢNG          ////////////////

    //Hàm load bảng dataTable dữ liệu
    function loadDataToTable(paramResponseJson) {
        gStt = 1;
        gStudentTable.clear();
        gStudentTable.rows.add(paramResponseJson);
        gStudentTable.draw();
    }

    //Hàm call API student
    function callApiServerStudents() {
        $.ajax({
            url: "https://62454a477701ec8f724fb923.mockapi.io/api/v1/" + "/students",
            dataType: "json",
            type: "GET",
            async: false,
            success: function(responseJson) {
                gStudentsData = responseJson;
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(ajaxContext);
            }
        });
    }

    //Hàm call API subject
    function callApiServerSubject() {

        $.ajax({
            url: "https://62454a477701ec8f724fb923.mockapi.io/api/v1/" + "/subjects",
            dataType: "json",
            type: "GET",
            async: false,
            success: function(responseJson) {
                gSubjectData = responseJson;
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(ajaxContext);
            }
        });
    }

    //Hàm call API grades
    function callApiAllGrades() {
        $.ajax({
            url: "https://62454a477701ec8f724fb923.mockapi.io/api/v1/" + "/grades/",
            dataType: "json",
            type: "GET",
            async: false,
            success: function(responseJson) {
                gGrades = responseJson;
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(ajaxContext);
            }
        });
    }

    //Hàm tạo option vào select FILTER
    function loadStudentSelect() {
        $("#student-select").append($("<option>", {
            text: "---Chọn học sinh---",
            value: 0
        }));
        for (var i = 0; i < gStudentsData.length; i++) {
            $("#student-select").append($("<option>", {
                text: gStudentsData[i].firstname + " " + gStudentsData[i].lastname,
                value: gStudentsData[i].id
            }));
        }
    }

    function loadSubjectSelect() {
        $("#subject-select").append($("<option>", {
            text: "---Chọn môn học---",
            value: 0
        }));
        for (var i = 0; i < gSubjectData.length; i++) {
            $("#subject-select").append($("<option>", {
                text: gSubjectData[i].subjectName,
                value: gSubjectData[i].id
            }));
        }
    }

    function loadSelectToModalUpdate(paramData) {
        for (var i = 0; i < gStudentsData.length; i++) {
            $("#update-student").append($("<option>", {
                text: gStudentsData[i].firstname + " " + gStudentsData[i].lastname,
                value: gStudentsData[i].id
            }));
            if (paramData.studentId == gStudentsData[i].id) {
                $("#update-student").val(gStudentsData[i].id);
            }
        }

        for (var i = 0; i < gSubjectData.length; i++) {
            $("#update-subject").append($("<option>", {
                text: gSubjectData[i].subjectName,
                value: gSubjectData[i].id
            }));
            if (paramData.subjectId == gSubjectData[i].id) {
                $("#update-subject").val(gSubjectData[i].id);
            }
        }

        for (var i = 0; i < gGrades.length; i++) {
            $("#update-mark").append($("<option>", {
                text: gGrades[i].grade,
                value: gGrades[i].id
            }));
            if (paramData.grade == gGrades[i].grade) {
                $("#update-mark").val(gGrades[i].grade);
            }
        }
        for (var i = 0; i < gGrades.length; i++) {
            $("#update-date").append($("<option>", {
                text: gGrades[i].examDate,
                value: gGrades[i].id
            }));
            if (paramData.examDate == gGrades[i].examDate) {
                $("#update-date").val(gGrades[i].examDate);
            }
        }

    }

    //Hàm render dữ liệu vào cột student
    function renderStudentData(paramData) {
        var vResultStudent = [];
        var vIsFound = false;
        var i = 0;
        while (!vIsFound && i < gStudentsData.length) {
            if (gStudentsData[i].id == paramData) {
                vIsFound = true;
                vResultStudent = gStudentsData[i].firstname + " " + gStudentsData[i].lastname;
            } else {
                i++;
            }
        }
        return vResultStudent;
        console.log(vResultStudent);
    }

    //Hàm render dữ liệu vào cột subject
    function renderSubjectData(paramData) {
        var vResultSubject = [];
        var vIsFound = false;
        var i = 0;
        while (!vIsFound && i < gSubjectData.length) {
            if (gSubjectData[i].id == paramData) {
                vIsFound = true;
                vResultSubject = gSubjectData[i].subjectName;
            } else {
                i++;
            }
        }
        return vResultSubject;
    }


    //Hàm select2
    function select2TranFormation() {
        $('#student-select').select2({
            theme: 'bootstrap4'
        });
        $('#subject-select').select2({
            theme: 'bootstrap4'
        });
        $('#update-student').select2({
            theme: 'bootstrap4'
        });
        $('#update-subject').select2({
            theme: 'bootstrap4'
        });
        $('#update-date').select2({
            theme: 'bootstrap4'
        });
        $('#update-mark').select2({
            theme: 'bootstrap4'
        });
        $('#create-select-student').select2({
            theme: 'bootstrap4'
        });
        $('#create-select-subject').select2({
            theme: 'bootstrap4'
        });
        $('#create-select-date').select2({
            theme: 'bootstrap4'
        });
        $('#create-select-grades').select2({
            theme: 'bootstrap4'
        });

    }

    // Load lại bảng khi tắt modal update
    $("#close-modal-x").on('click', function() {
        onBtnClickClose();
    })
    $("#btn-update-cancel").on('click', function() {
        onBtnClickCancel();
    })

    function onBtnClickClose() {
        location.reload();
    }

    function onBtnClickCancel() {
        location.reload();
    }