   /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

   var gCountryObjects = [{
       "value": "VN",
       "text": "Việt Nam"
   }, {
       "value": "USA",
       "text": "USA"
   }, {
       "value": "AUS",
       "text": "Australia"
   }, {
       "value": "CAN",
       "text": "Canada"
   }];

   //Khai báo biến mảng hằng số chứa danh sách tên các thuộc tính
   const gUSER_COLS = ["id", "firstname", "lastname", "country", "subject", "customerType", "registerStatus", "action"];

   //Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
   const gUSER_ID_COL = 0;
   const gFIRST_NAME_COL = 1;
   const gLAST_NAME_COL = 2;
   const gCOUNTRY_COL = 3;
   const gSUBJECT_COL = 4;
   const gCUSTOMER_TYPE_COL = 5;
   const gREGISTER_STATUS_COL = 6;
   const gACTION_COL = 7;

   //Đổ dữ liệu vào bảng và thực hiện nút Sửa(Edit), Xóa(Delete)
   $('#user-table').DataTable({
       columns: [{
           data: gUSER_COLS[gUSER_ID_COL]
       }, {
           data: gUSER_COLS[gFIRST_NAME_COL]
       }, {
           data: gUSER_COLS[gLAST_NAME_COL]
       }, {
           data: gUSER_COLS[gCOUNTRY_COL]
       }, {
           data: gUSER_COLS[gSUBJECT_COL]
       }, {
           data: gUSER_COLS[gCUSTOMER_TYPE_COL]
       }, {
           data: gUSER_COLS[gREGISTER_STATUS_COL]
       }, {
           data: gUSER_COLS[gACTION_COL]
       }, ],

       columnDefs: [{
           //Định nghĩa lại cột Country
           targets: gCOUNTRY_COL,
           render: function(data) {
               return getCountryTextByCountryValue(data)
           }
       }, { //Định nghĩa lại cột Action
           targets: gACTION_COL,
           className: "text-center",
           defaultContent: `
        <button class="btn btn-primary btn-edit"> Sửa</button>
        <button class="btn btn-danger btn-delete" data-toggle='modal' > Xóa</button>
        `
       }],
       autoWidth: false
   });


   //Khai báo biến mảng chứa dữ liệu users
   var gListUsers = [];

   var gUserObject = "";
   // const gNOT_SELECT_COUNTRY = "NOT_SELECT_COUNTRY";



   /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */


   $(document).ready(function() {
       onPageLoading();

       // 2 - C: Gán sự kiện Create - Thêm mới user
       $("#btn-add-user").on("click", function() {
           onBtnAddNewUserClick();
       });
       // gán sự kiện cho nút Create Voucher (trên modal)
       $("#btn-create-user").on("click", function() {
           onBtnCreateUserClick();
       });




       //3 - U: gán sự kiện Update - Sửa 1 user
       $("#user-table").on("click", ".btn-edit", function() {
           onBtnEditClick(this);
       });

       // gán sự kiện cho nút Update Voucher (trên modal)
       $("#btn-update-user").on("click", function() {
           onBtnUpdateUserClick();
       });




       // 4 - D: gán sự kiện Delete - xóa 1 user
       $("#user-table").on("click", ".btn-delete", function() {
           onBtnDeleteUserClick(this);
       });

       // gán sự kiện cho nút Update Voucher (trên modal)
       $("#btn-delete-user").on("click", function() {
           onBtnConfirmDeleteUserClick();
       });



   });


   /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */



   // Hàm Load Bảng cho trang
   function onPageLoading() {
       "use strict";

       callApiGetAllUsers();

       // 1 - R: Read / Load user to DataTable
       loadDataToUserTable(gListUsers);

   }



   // ADD NEW
   // Hàm sự kiện click nút thêm mới -- Add User
   function onBtnAddNewUserClick() {

       // Hiển thị modal addn new lên
       $("#insert-user-modal").modal("show");

   }



   //UPDATE DATA USER
   //hàm xử lý sự kiện click nút sửa
   function onBtnEditClick(paramButtonElement) {
       "use strict";
       console.log("Button Edit Click!!!");

       // lưu thông tin User đang được edit vào biến toàn cục
       gUserObject = getUserFormButton(paramButtonElement)
       console.log(gUserObject.id)
           // load dữ liệu vào các trường dữ liệu trong modal
       getUserUpdateData(gUserObject);

       // hiển thị modal update lên
       $('#update-user-modal').modal('show');

   }


   // DELETE DATA USER
   //hàm xử lý sự kiện click nút xóa
   function onBtnDeleteUserClick(paramButtonElement) {
       "use strict";
       console.log("Button Delete Click!!!");

       // lưu thông tin User đang được delete vào biến toàn cục
       gUserObject = getUserFormButton(paramButtonElement);

       // hiển thị modal delete lên
       $('#delete-user-modal').modal('show');

   }





   /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/



   // CÁC HÀM ADD NEW CỦA BẢNG MODAL

   // hàm xử lý sự kiện click nút insert
   function onBtnCreateUserClick() {
       "use strict";

       //Khai báo đối tượng chứa user data
       var vUserObj = {
           firstname: "",
           lastname: "",
           subject: "",
           country: ""
       }

       //B1 : thu thập dữ liệu
       getUserDataCreate(vUserObj);

       //B2 : Validate insert
       var vIsUserValidate = validateInsertData(vUserObj);
       if (vIsUserValidate) {

           //B3 : insert user
           CallApiAddNewUserApi(vUserObj)

           //B4 : xử lý front-end
           //gọi api lấy danh sánh user
           callApiGetAllUsers();

           //Load lại vào bảng (table)
           loadDataToUserTable(gListUsers);

           //Xóa trắng dữ liệu trên model
           resertCreateUserForm();

           //Tắt ẩn modal form create form create user
           $("#insert-user-modal").modal("hide");


       }
   };


   // hàm thu thập dữ liệu để insert user
   function getUserDataCreate(paramUserObj) {
       "use strict";

       paramUserObj.firstname = $("#inp-firstname").val().trim();
       paramUserObj.lastname = $("#inp-lastname").val().trim();
       paramUserObj.subject = $("#inp-subject").val().trim();
       paramUserObj.country = $("#select-country").val();

   }

   //Hàm validate data khi insert
   function validateInsertData(paramUserObj) {
       "use strict";

       if (paramUserObj.firstname == "") {
           alert("First name cần nhập");
           return false;
       }
       if (paramUserObj.lastname == "") {
           alert("Last name cần nhập");
           return false;
       }
       if (paramUserObj.subject == "") {
           alert("Subject cần nhập");
           return false;
       }
       if (paramUserObj.country == "NOT_SELECT_COUNTRY") {
           alert("Country name cần nhập");
           return false;
       }
       return true;
   }


   // Hàm gọi API insert user
   function CallApiAddNewUserApi(paramUserObj) {
       "use strict";

       $.ajax({
           url: "http://42.115.221.44:8080/crud-api/users/",
           type: "POST",
           async: false,
           data: JSON.stringify(paramUserObj),
           contentType: "application/json",
           success: function(res) {
               console.log(res);
               alert("Thêm user thành công!");
           },
           error: function(error) {
               alert(error.responseText);
           }
       });

   }

   //Hàm xóa trắng bảng form create user
   function resertCreateUserForm() {

       $("#inp-firstname").val("");
       $("#inp-lastname").val("");
       $("#inp-subject").val("");
       $("#select-country").val("NOT_SELECT_COUNTRY");

   }




   // CÁC HÀM ADD NEW CỦA BẢNG MODAL


   // hàm thu thập dữ liệu vào bảng update user trong modal
   function getUserUpdateData(paramUserObj) {
       "use strict";

       $("#edit-inp-firstname").val(paramUserObj.firstname);
       $("#edit-inp-lastname").val(paramUserObj.lastname);
       $("#edit-inp-subject").val(paramUserObj.subject);
       $("#edit-select-country").val(paramUserObj.country);
       $("#edit-select-customerType").val(paramUserObj.customerType);
       $("#edit-select-registerStatus").val(paramUserObj.registerStatus);

   }


   // Hàm sự kiện click nút Update trong modal
   function onBtnUpdateUserClick() {
       var vUpdateUser = {
           firstname: "",
           lastname: "",
           subject: "",
           country: "",
           customerType: "",
           registerStatus: ""

       }

       // bước 1 lấy dữ liệu từ form nhập
       getUpdateUserData(vUpdateUser);

       // bước 2 kiểm tra thông tin
       var vCheckValidateUpdate = validateUpdateData(vUpdateUser);

       if (vCheckValidateUpdate) {

           //B3: Update Data User

           CallApiUpdateUserApi(vUpdateUser);

           //B4: xử lý front-end
           callApiGetAllUsers();

           //Load lại vào bảng (table)
           loadDataToUserTable(gListUsers);

           //Xóa trắng dữ liệu trên model
           resertEditUserForm();

           $('#update-user-modal').modal('hide');

       }
   }


   // Hàm lấy dữ liệu từ form
   function getUpdateUserData(paramUserUpdate) {

       paramUserUpdate.firstname = $("#edit-inp-firstname").val().trim();
       paramUserUpdate.lastname = $("#edit-inp-lastname").val().trim();
       paramUserUpdate.subject = $("#edit-inp-subject").val().trim();
       paramUserUpdate.country = $("#edit-select-country").val();
       paramUserUpdate.customerType = $("#edit-select-customerType").val();
       paramUserUpdate.registerStatus = $("#edit-select-registerStatus").val();

   }

   //Hàm validate data khi Update
   function validateUpdateData(paramUserUpdate) {
       "use strict";

       if (paramUserUpdate.firstname == "") {
           alert("First name cần nhập");
           return false;
       }
       if (paramUserUpdate.lastname == "") {
           alert("Last name cần nhập");
           return false;
       }
       if (paramUserUpdate.subject == "") {
           alert("Subject cần nhập");
           return false;
       }

       return true;

   }

   //Call API Update
   function CallApiUpdateUserApi(paramUserObj) {
       "use strict";

       $.ajax({
           url: "http://42.115.221.44:8080/crud-api/users/" + gUserObject.id,
           type: "PUT",
           async: false,
           data: JSON.stringify(paramUserObj),
           contentType: "application/json",
           success: function(updateUser) {
               console.log(updateUser);
               alert("Update User thành công!");
           },
           error: function(error) {
               alert(error.responseText);

           }
       });
   }

   // Hàm xáo trắng modal Update
   function resertEditUserForm() {
       $("#edit-inp-firstname").val("");
       $("#edit-inp-lastname").val("");
       $("#edit-inp-subject").val("");
       $("#edit-select-country").val("NOT_SELECT_COUNTRY");
       $("#edit-select-customerType").val("NOT_SELECT_CUSTOMERTYPE");
       $("#edit-select-registerStatus").val("NOT_SELECT_REGISTERSTATUS");

   }



   // CÁC HÀM ADD NEW CỦA BẢNG MODAL

   // hàm xử lý sự kiện click nút DELETE
   function onBtnConfirmDeleteUserClick() {

       // B1: thu thập dữ liệu (ko có)
       // B2: validate (ko có)

       // B3: xóa User Data
       callApiDeleteUser()

       // B4: hiển thị dữ liệu lên front-end (load lại table, ẩn modal)

       //gọi api lấy danh sánh user
       callApiGetAllUsers();

       //Load lại vào bảng (table)
       loadDataToUserTable(gListUsers);

       // ẩn modal confirm xóa đi
       $('#delete-user-modal').modal('hide');

   }


   //Call API DELETE
   function callApiDeleteUser() {
       "use strict";

       $.ajax({
           url: "http://42.115.221.44:8080/crud-api/users/" + gUserObject.id,
           method: "DELETE",
           async: false,
           contentType: "application/json",
           dataType: "json",
           success: function(deleteUser) {
               confirm("Xóa thông tin thành công !!!");
               console.log(deleteUser);

           },
           error: function(ajaxContext) {
               console.log(ajaxContext.responseText);
               confirm("Xóa thông tin không thành công !")

           }
       })

   }





   //HÀM DÙNG CHUNG CHO UPDATE-MODAL VÀ DELeTE-MODAL

   // hàm dựa vào button detail (edit or delete) xác định đc id User
   function getUserFormButton(paramButtonElement) {
       var vRowClick = $(paramButtonElement).parents("tr"); // xác định tr chứa nút bấm dc click
       var vTable = $("#user-table").DataTable(); // tạo biến truy xuất đến đataTable
       var vDataRow = vTable.row(vRowClick).data(); //lấy dữ liệu của hàng chứa nút bấm được click
       console.log("%cID của user tương ứng: " + vDataRow.id, "color:blue");
       console.log("");

       return vDataRow;

   }




   // CÁC HÀM DÙNG CHUNG CHO LOAD BẢNG

   // thêm data Country thông qua value
   function getCountryTextByCountryValue(paramCountry) {
       "use strict";

       var vTextCountry = "";
       for (let bI = 0; bI < gCountryObjects.length; bI++) {
           if (gCountryObjects[bI].value == paramCountry) {
               vTextCountry = gCountryObjects[bI].text;
           }
       };
       return vTextCountry;
   }

   //Hàm gọi API lấy danh sách user
   function callApiGetAllUsers() {
       "use strict";

       $.ajax({
           async: false,
           url: "http://42.115.221.44:8080/crud-api/users/",
           type: "GET",
           dataType: "json",
           success: function(res) {
               //lấy response trả về và gán cho biến gListUser
               gListUsers = res;
           },
           error: function(error) {
               console.log(error.responseText);
           }
       })
   }

   //Load người dùng vào DataTable
   // input/start:
   //-paramObjectUsers: mảng người dùng
   //output/end: hiện thị dữ liệu vào mảng

   function loadDataToUserTable(paramObjectUsers) {
       console.log(paramObjectUsers);
       var vTable = $("#user-table").DataTable();
       vTable.clear();
       vTable.rows.add(paramObjectUsers);
       vTable.draw();
   }