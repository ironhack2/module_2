"use strict";
$(document).ready(function() {

    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gDataOrders = []; //Khai báo biển mảng (biến toàn cục) để chứa thông tin đơn hàng

    var gId;
    var gOrderId;
    var gPizzaSize = ["S", "M", "L", "Samll", "Medium", "Large"];

    const gORDER = ["orderId", "kichCo", "loaiPizza", "idLoaiNuocUong", "thanhTien", "hoTen", "soDienThoai", "trangThai", "action"];

    //Khai báo các cột của ĐATATABLE
    const gCOL_ORDER_ID = 0;
    const gCOL_KICH_CO = 1;
    const gCOL_LOAI_PIZZA = 2;
    const gCOL_ID_LOAI_NUOC_UONG = 3;
    const gCOL_THANH_TIEN = 4;
    const gCOL_HO_TEN = 5;
    const gCOL_SO_DIEN_THOAI = 6;
    const gCOL_TRANG_THAI = 7;
    const gCOL_ACTION = 8;

    //định nghĩa table - chưa có data
    $('#order-table').DataTable({
        "columns": [{
            "data": gORDER[gCOL_ORDER_ID]
        }, {
            "data": gORDER[gCOL_KICH_CO]
        }, {
            "data": gORDER[gCOL_LOAI_PIZZA]
        }, {
            "data": gORDER[gCOL_ID_LOAI_NUOC_UONG]
        }, {
            "data": gORDER[gCOL_THANH_TIEN]
        }, {
            "data": gORDER[gCOL_HO_TEN]
        }, {
            "data": gORDER[gCOL_SO_DIEN_THOAI]
        }, {
            "data": gORDER[gCOL_TRANG_THAI]
        }, {
            "data": gORDER[gCOL_ACTION]
        }],

        "columnDefs": [{
            "targets": gCOL_ACTION,
            "defaultContent": `
                <button class="btn-detail btn btn-primary">Chi tiết</button>
                `
        }]

    })



    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */

    //Hàm chạy khi trang được load
    onPageLoading();

    getDrinkListAjaxClick();
    getPizzaSizeList(gPizzaSize);


    //gán event handler (sự kiện) cho nút chi tiết
    $('#order-table').on("click", ".btn-detail", function() {
        onBtnDetailClick(this); //this là button được ấn
    });


    //gần event hanlder (sự kiện) cho nút lọc
    $("#btn-filter").on('click', function() {
        onBtnFilterDataClick();
    })


    // HÀM SỰ KIỆN BUTTON TRONG MODAL

    //nút update trang thái confirm
    $('#btn-confirm').on('click', function() {
        onBtnUpdateConfirmClick()
    })


    //nút update trang thái cancel
    $('#btn-cancel').on('click', function() {
        onBtnCancelConfirmClick()
    })


    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

    function onPageLoading() {
        //lấy data từ sever

        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
            type: "GET",
            dataType: "json",
            success: function(responseObj) {
                gDataOrders = responseObj;
                console.log(gDataOrders);

                //gọi hàm load dữ liệu bảng
                loadDataToTable(gDataOrders)
            },
            error: function(error) {
                console.assert(error.responseText);
            }
        });

    }

    // TÌM KIẾM THÔNG TIN ORDER THEO NAME

    //Hàm xử lý nút Filter
    function onBtnFilterDataClick() {
        "use strict";

        var vStatus = $('#select-status').val();
        var vPizza = $('#select-pizza').val();
        var vDataFilter = gDataOrders.filter(function(paramOrder) {

            if (paramOrder.trangThai != null && paramOrder.loaiPizza != null) {
                return ((paramOrder.trangThai.includes(vStatus) || vStatus === "all") &&
                    (paramOrder.loaiPizza.includes(vPizza) || vPizza === "all"));
            }
        });
        //gọi hàm load bảng
        loadDataToTable(vDataFilter);
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/


    //Hàm xử lý load(đổ) dữ liệu vào bảng
    function loadDataToTable(paramResponseObj) {
        "use strict";

        var vUserTable = $('#order-table').DataTable();

        //xóa toàn bộ dữ liêu đang có
        vUserTable.clear();

        //Cập nhập data cho bảng
        vUserTable.rows.add(paramResponseObj);

        //Cập nhật lại giao điện hiển thị bảng
        vUserTable.draw();

    }



    // XỬ LÝ NUT CHI TIẾT VÀ MODAL

    //Hàm xử lý khi ấn nút chi tiết
    function onBtnDetailClick(paramElement) {
        "use strict";

        var vRowSelected = $(paramElement).closest("tr");
        var vTable = $("#order-table").DataTable();
        var vDataRow = vTable.row(vRowSelected).data();
        gId = vDataRow.id;
        gOrderId = vDataRow.orderId;
        console.log("ID:" + gId);
        console.log("Order ID:" + gOrderId);
        getAjaxDetailOrder(gOrderId);
        $('#order-modal').modal('show');
    }


    // LOAD DỮ LIỆU VÀO MODAL

    //Hàm gọi API order theo orderID
    function getAjaxDetailOrder(paramOrderId) {
        "use strict";
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + paramOrderId,
            type: "GET",
            dataType: "json",
            success: function(responseObj) {
                handleOrderDetail(responseObj);
                console.log(responseObj);
            },
            error: function(ajaxContext) {
                alert(ajaxContext.responseText);
            }
        })
    }

    //Hàm đổ dữ liệu vào modal
    function handleOrderDetail(paramResponseObj) {
        "use strict";

        $('#inp-id').val(paramResponseObj.id);
        $('#inp-orderid').val(paramResponseObj.orderId);
        $('#select-pizzasize').val(paramResponseObj.kichCo.toLowerCase());
        $('#inp-duongkinh').val(paramResponseObj.duongKinh);

        $('#inp-suon').val(paramResponseObj.suon);
        $('#inp-salad').val(paramResponseObj.salad);
        $('#select-pizzatype').val(paramResponseObj.loaiPizza.toLowerCase());
        $('#inp-idvoucher').val(paramResponseObj.idVourcher);

        $('#inp-thanhtien').val(paramResponseObj.thanhTien);
        $('#inp-giamgia').val(paramResponseObj.giamGia);
        $('#select-drink').val(paramResponseObj.idLoaiNuocUong);
        $('#inp-soluongdrink').val(paramResponseObj.soLuongNuoc);

        $('#inp-fullname').val(paramResponseObj.hoTen);
        $('#inp-email').val(paramResponseObj.email);
        $('#inp-phone').val(paramResponseObj.soDienThoai);
        $('#inp-address').val(paramResponseObj.diaChi);
        $('#inp-message').val(paramResponseObj.loiNhan);

        $('#inp-orderday').val(paramResponseObj.ngayTao);
        $('#inp-repairday').val(paramResponseObj.ngayCapNhat);
    }

    // LOAD DỮ LIỆU VÀO SELECT - MODAL

    //Hàm tạo option cho select đồ uống
    function handleDrinkList(paramDrink) {
        "use strict";

        $.each(paramDrink, function(i, item) {
            $("#select-drink").append($('<option>', {
                text: item.tenNuocUong,
                value: item.maNuocUong,
            }))
        })
    }

    //Hàm gọi api select drink
    function getDrinkListAjaxClick() {
        "use strict";

        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
            dataType: "json",
            type: "GET",
            success: function(responseObj) {
                handleDrinkList(responseObj);
            },
            error: function(ajaxContext) {
                console.log(ajaxContext.responseText);
                alert(ajaxContext.responseText);
            }

        })
    }

    //Hàm tạo option cho select cho select pizza size
    function getPizzaSizeList(paramPizza) {
        "use strict";

        $.each(paramPizza, function(i, item) {
            $('#select-pizzasize').append($('<option>', {
                text: item,
                value: item.toLowerCase()
            }))
        })
    }



    // UPDATE MODAL

    function onBtnUpdateConfirmClick() {

    }

    function onBtnUpdateCancelClick() {

    }


});